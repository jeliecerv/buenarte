﻿<?php  

 // no direct access 

 defined( '_JEXEC' ) or die( 'Restricted access' );?>

<!DOCTYPE html>

<html>

<head>
<jdoc:include type="head" />
<meta charset="utf-8">
<link href="templates/buenarte_home/css/estilo.css" rel="stylesheet"/>
<link href="templates/buenarte_home/css/jqueryui.css" rel="stylesheet"/>
<script src="templates/buenarte_home/script/jquery.js"></script>
<!--<script src="templates/buenarte_home/script/codigo.js"></script>
<script src="templates/hill_home/script/jqueryui.js"></script>
<script src="templates/hill_home/script/jquery.nav.js"></script>
<script src="templates/hill_home/script/jquery.scrollTo.js"></script>
<script src="templates/hill_home/script/jquery.jparallax.js"></script>-->



<script>


</script>





<title>Buenarte</title>



</head>



<body>

 <header>
  <div id="sec1">
    <div id="logo"><jdoc:include type="modules" name="logo" /></div>
    <div id="info"><jdoc:include type="modules" name="info" /></div>
  </div>
  
  <div id="sec2" class="home_layout">
    <nav><jdoc:include type="modules" name="menu" /></nav>
    <div id="redes"><jdoc:include type="modules" name="redes" /></div>
  </div>  
 </header>

 

 <section>
  <div id="sec3">
    <div id="banner"><jdoc:include type="modules" name="banner" />
      <div id="pop"><jdoc:include type="modules" name="pop" /></div>
    </div>
    <div id="botones"><jdoc:include type="modules" name="botones" /></div>
  </div>
  
  <div id="sec4"> 
    <div id="caja_face"><jdoc:include type="modules" name="cajaface" /></div>
    <div id="info_federacion"><jdoc:include type="modules" name="federacion" /></div>
  </div>

  <div id="sec5"> 
    <div id="img_mitico"><jdoc:include type="modules" name="imgmitico" /></div>
    <div id="federacion_info"><jdoc:include type="modules" name="infofederacion" /></div>
  </div>
 </section>

 

 <footer>
  <div id="info_footer"><jdoc:include type="modules" name="footer" /></div>
 </footer>

</body>



</html>