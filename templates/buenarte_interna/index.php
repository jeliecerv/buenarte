﻿<?php  

 // no direct access 

 defined( '_JEXEC' ) or die( 'Restricted access' );?>

<!DOCTYPE html>

<html xmlns:fb="https://www.facebook.com/2008/fbml">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object#">
<jdoc:include type="head" />
<meta charset="utf-8">
<link rel="stylesheet" href="templates/buenarte_interna/css/specimen_files/specimen_stylesheet.css"/>
<link href="templates/buenarte_interna/css/estilo.css" rel="stylesheet"/>
<link rel="image_src" href="images/logo.png" />
<link href="templates/buenarte_interna/css/jqueryui.css" rel="stylesheet"/>
<script src="templates/buenarte_interna/script/jquery.js"></script>
<script src="templates/buenarte_interna/css/specimen_files/easytabs.js"></script>
<!--<script src="templates/buenarte_home/script/codigo.js"></script>
<script src="templates/hill_home/script/jqueryui.js"></script>
<script src="templates/hill_home/script/jquery.nav.js"></script>
<script src="templates/hill_home/script/jquery.scrollTo.js"></script>
<script src="templates/hill_home/script/jquery.jparallax.js"></script>-->



<script>



</script>





<title>Buenarte</title>


<!-- Metas Facebook -->
<meta property="og:title" content="Buenarte Buendia" />
<meta property="og:description" content="Concurso de imagenes" />
<meta property="og:image" content="images/logo.png" />

</head>



<body>
<div id="fb-root"></div>
  <script>
    function megusta()
  {
      FB.api(
        '/me/buenarte:like',
        'post',
        { object: 'object=http://samples.ogp.me/226075010839791' },
        function(response) {
           if (!response || response.error) {
              alert('Error occured');
           } else {
              alert('Cook was successful! Action ID: ' + response.id);
           }
        });
  }
    
    
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '  387822187969071', // App ID
        status     : true, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
      });
    };

    // Load the SDK Asynchronously
    (function(d){
      var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
      js = d.createElement('script'); js.id = id; js.async = true;
      js.src = "//connect.facebook.net/es_ES/all.js";
      d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
  </script>
 <header>
  <div id="sec1">
    <div id="logo"><jdoc:include type="modules" name="logo" /></div>
    <div id="info"><jdoc:include type="modules" name="info" /></div>
  </div>
  
  <div id="sec2">
    <nav><jdoc:include type="modules" name="menu" /></nav>
    <div id="redes"><jdoc:include type="modules" name="redes" /></div>
  </div>  
 </header>

 

 <section>
   
  <div id="sec3">
    <div>
      <div id="contento">
        <div style="color:#ff5d05; clear:both;">
          <jdoc:include type="message" />
        </div>
        <jdoc:include type="component" />
        <div id="galleries"><jdoc:include type="modules" name="galleries" /></div>
      </div>
    </div>
  <div id="botones"><jdoc:include type="modules" name="botones" /></div>
    
    
  </div>
   
 </section>
  
 
<br><br>
 <footer>
  <div id="info_footer"><jdoc:include type="modules" name="footer" /></div>
 </footer>

</body>



</html>