<?php
/*------------------------------------------------------------------------
# mod_sitesponsors - Joomla! nice messages module
# ------------------------------------------------------------------------
# author    Joomla Dancer - Burak Yenigun, support@joomladancer.com
# copyright Copyright (C) 2010 JoomlaDancer.com. All Rights Reserved.
# @license - GNU/GPL - http://www.gnu.org/licenses/gpl-2.0.html
# @credits - jquery.toastmessage-min.js is developed by akquinet A.G and licensed under the Apache License 2.0 - http://www.apache.org/licenses/LICENSE-2.0.html
# Websites: http://www.JoomlaDancer.com
# Technical Support:  Forum - http://www.joomladancer.com/forum/categories.html
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');
require_once (dirname(__FILE__).DS.'helper.php');
// get first parameter
$jqueryload  	= $params->get('jqueryload', '1');
$loadafter 		= $params->get('loadafter', '1000');
$position 		= $params->get('position', 'middle-center');
$messagebody	= $params->get('messagebody', 'Hi, This is latest work from JoomlaDancer');
$sticky  		= $params->get('sticky', '1');
$staytime  		= $params->get('staytime', '3000');
$repeat 		= $params->get('repeat', '1');
$inffectduration 		= $params->get('inffectduration', '1000');
$shorturl 		= $params->get('shorturl', '1');

// style parameters
$width 			= $params->get('width', '350');
$background 	= $params->get('background', '#333');
$border 	= $params->get('border', '2px solid #0c85a4');
$borderradius 	= $params->get('borderradius', '15px');
$opacity 	= $params->get('opacity', '1');

// new styles
$viewchange 	= $params->get('viewchange', 'default');
// end parameters

// get page title
global $mainframe;
$page_title = $mainframe->getPageTitle();


// modify body
$modifyMessageBody = ereg_replace( "\n", "<br/>", $messagebody);
switch ($viewchange){
case "social":

$url =& JFactory::getURI();
$niceurl = $url->toString();
$niceurl = str_replace( '&amp;', '&', $niceurl );

if($shorturl == 1):
$niceurl  = modNiceMessagesHelper::getShortUrl($niceurl);
$metadesc = modNiceMessagesHelper::getDesc();
$metakeyw = modNiceMessagesHelper::getKeywords();
endif;
$modifyMessageBody = "<img src=\"modules/mod_nicemessages/images/social/sharing.png\" \"><br/>";
$modifyMessageBody.= "<a href=\"http://facebook.com/share.php?u=".$niceurl."\"title=\"Share on Facebook!\" target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/face.png\" \"></a>";
$modifyMessageBody.= "<a href=\"http://twitter.com/home?status=".$page_title." ".$niceurl."\"title=\"Share on Twitter!\" target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/twitter.png\" \"></a>";
$modifyMessageBody.= "<a href=\"http://www.google.com/buzz/post?url=".$niceurl."&type=small-count\"target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/googlebuzz.png\" \"></a>";
$modifyMessageBody.= "<a href=\"http://www.linkedin.com/shareArticle?mini=true&url=".$niceurl."&title=".$page_title."&source=".$niceurl."\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/linkedin.png\" \"></a>";
$modifyMessageBody.= "<a href=\"http://delicious.com/save\" onclick=\"window.open(\'http://delicious.com/save?v=5&amp;noui&amp;jump=close&amp;url=\'+encodeURIComponent(\'".$niceurl."\')+\'&amp;title=\'+encodeURIComponent(\'".$page_title."\'),\'delicious\', \'toolbar=no,width=550,height=550\'); return false;\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/delicious.png\" \"></a>";
$modifyMessageBody.= "<a href=\"http://www.stumbleupon.com/submit?url=".$niceurl."\"target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/stumbleupon.png\" \"></a>";
$modifyMessageBody.= "<a href=\"http://google.com/bookmarks/mark?op=edit&bkmk=".$niceurl."&title=".$page_title."&annotation=".$metadesc."&labels=".$metakeyw."\"target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/google.png\" \"></a>";
$modifyMessageBody.= "<a href=\"http://myweb2.search.yahoo.com/myresults/bookmarklet?u=".$niceurl."&d=".$page_title."&tag[0]=".$metakeyw."\" target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/yahoo.png\" \"</a>";
$modifyMessageBody.= "<a href=\"http://digg.com/submit?url=".$niceurl."\"target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/digg.png\" \"</a>";
$modifyMessageBody.= "<a href=\"http://technorati.com/faves?add=".$niceurl."\"target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/technorati.png\" \"</a>";
$modifyMessageBody.= "<a href=\"http://www.reddit.com/submit?url=".$niceurl."\"target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/reddit.png\" \"</a>";
$modifyMessageBody.= "<a href=\"http://friendfeed.corank.com/submit?url=".$niceurl."&title=".$page_title."&source=k \"target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/friendfeed.png\" \"</a>";
$modifyMessageBody.= "<a href=\"http://www.newsvine.com/_tools/seed&save?u=".$niceurl."&h=".$page_title."\"target=\"_blank\"><img class=\"shareicon\" src=\"modules/mod_nicemessages/images/social/newsvine.png\" \"</a>";

$border="0px";
$background="none"; ?>
<style>
.toast-item{
padding:5px 0 0 0;}
.toast-container{
width:400px; height:1px;
}
</style>
<?php
break;
}

// add style and scripts
$document = &JFactory::getDocument();
if($jqueryload):
$document->addScript( 'modules/mod_nicemessages/js/jquery-1.5.min.js' );
endif;
$document->addScript( 'modules/mod_nicemessages/js/jquery.toastmessage-min.js' );
$document->addStyleSheet( 'modules/mod_nicemessages/css/style.css' );
?>
<style>
.toast-container{width:<?php echo $width;?>px;}
.toast-item{background:<?php echo $background;?>;
border:<?php echo $border;?>;
-moz-border-radius:<?php echo $borderradius;?>px;-webkit-border-radius:<?php echo $borderradius;?>px;
opacity:<?php echo $opacity;?>;
}
</style>
<?php
$domain = JURI::root();
if($repeat){
if (empty($_COOKIE['nicemessage'])):
?>
<script type="text/javascript">
var jq = jQuery.noConflict();
  jq(document).ready(function () {
  setTimeout("showSticky()",<?php echo $loadafter;?>);
});
</script>


<script type="text/javascript">
var jq = jQuery.noConflict();
	function showSuccessToast() {
        jq().toastmessage('showSuccessToast', "...");
    }
	
    function showSticky() {
        jq().toastmessage('showToast', {
			inEffectDuration:'<?php echo $inffectduration; ?>',
			stayTime: '<?php echo $staytime; ?>',
            text     : '<?php echo trim($modifyMessageBody); ?>',
            sticky   : <?php if ( $sticky ){ echo "true"; } else { echo "false"; } ?>,
            position : '<?php echo $position; ?>',
            type     : '',
            closeText: '',
            close    : function () {
                console.log("closed ...");
            }
        });
}
</script>


   <?php 
   
   setcookie("nicemessage", 1, NULL); 
   
   endif; }
		else { ?>
		<script type="text/javascript">
		var jq = jQuery.noConflict();
  jq(document).ready(function () {
  setTimeout("showSticky()",<?php echo $loadafter;?>);
});
</script>


<script type="text/javascript">

	function showSuccessToast() {
        jq().toastmessage('showSuccessToast', "...");
    }
	
    function showSticky() {
        jq().toastmessage('showToast', {
			inEffectDuration:'<?php echo $inffectduration; ?>',
			stayTime: '<?php echo $staytime; ?>',
            text     : '<?php echo trim($modifyMessageBody); ?>',
            sticky   : <?php if ( $sticky ){ echo "true"; } else { echo "false"; } ?>,
            position : '<?php echo $position; ?>',
            type     : '',
            closeText: '',
            close    : function () {
                console.log("closed ...");
            }
        });
}
</script> <?php } ?>

