<?php
/*------------------------------------------------------------------------
# mod_sitesponsors - joomla nice messages
# ------------------------------------------------------------------------
# author    Joomla Dancer - Burak Yenigun, support@joomladancer.com
# copyright Copyright (C) 2010 JoomlaDancer.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.JoomlaDancer.com
# Technical Support:  Forum - http://www.joomladancer.com/forum/categories.html
-------------------------------------------------------------------------*/
// no direct access
?>
<?php
defined('_JEXEC') or die('Restricted access');

	class modNiceMessagesHelper{
	
		function getShortUrl($url) {
			
			$api_url = 'http://tinyurl.com/api-create.php?url='.$url;
				
				$ch = curl_init();
				$timeout = 5;
				curl_setopt($ch,CURLOPT_URL,$api_url);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
				$data = curl_exec($ch);
				curl_close($ch);
			
			return $data;
			
// end getShortUrl()
	}
	
	function getKeywords(){
	
	   $doc =& JFactory::getDocument();
	   $sitekeywords =  $doc->getMetaData('Keywords');
	   return $sitekeywords;
	   
	}
	
	function getDesc(){
	
	   $doc =& JFactory::getDocument();
	   $siteDesc = $doc->getDescription();
	   return $siteDesc;
	}
// end class	
}
?>
