<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_login
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
?>
<style>
#ff{background:#edeeef; color:#492f26;border: 2px solid #FCFCFC; width: 369px; height: 244px; font-size:13px; padding: 19px;margin-top: -2px;
margin-left: 5px;box-shadow: 0 0 13px #CDCBCB; -webkit-box-shadow: 0 0 13px #CDCBCB; -moz-box-shadow: 0 0 13px #CDCBCB; -ms-box-shadow: 0 0 13px #CDCBCB; -o-box-shadow: 0 0 13px #CDCBCB;}
</style>

<?php if ($type == 'logout') : ?>
<div id="tituloins"><img src="images/titubien.png" /></div>
<form style="background:#edeeef; border: 2px solid #FCFCFC; height: 300px; font-size:13px; padding: 0 19px 19px 0;margin-top: -6px;
width: 389px;
margin-left: 5px;box-shadow: 0 0 13px #CDCBCB; -webkit-box-shadow: 0 0 13px #CDCBCB; -moz-box-shadow: 0 0 13px #CDCBCB; -ms-box-shadow: 0 0 13px #CDCBCB; -o-box-shadow: 0 0 13px #CDCBCB;" action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form">
<?php if ($params->get('greeting')) : ?>
	
<?php endif; ?>
<div><img src="images/imghola.png" /></div>
	<div class="logout-button">
	<br>

		<br>
		<br>
		
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<?php else : ?>
<form style="margin-top: 19px;" action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" >
	<?php if ($params->get('pretext')): ?>
		<div class="pretext">
		<p><?php echo $params->get('pretext'); ?></p>
		</div>
	<?php endif; ?>
	<div id="tituloins"><img src="images/titulore.png" /></div>
	<div id="ff">
	<p id="form-login-username">
		<label for="modlgn-username"><?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?></label><br>
		<input id="modlgn-username" type="text" name="username" class="inputbox"  size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn-passwd"><?php echo JText::_('JGLOBAL_PASSWORD') ?></label><br>
		<input id="modlgn-passwd" type="password" name="password" class="inputbox" size="18"  />
	</p>
	<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
	<p id="form-login-remember" style="margin-top:10px;">
		<label for="modlgn-remember"><?php echo JText::_('MOD_LOGIN_REMEMBER_ME') ?></label>
		<input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
	</p>
	<?php endif; ?>
	
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.login" />
	<input type="hidden" name="return" value="<?php echo $return; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	<ul style="margin-top:10px;list-style:none;">
		<li>
			<a style="color:#492f26;" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
			<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>
		</li>
		<li>
			<a style="color:#492f26;" href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
			<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_USERNAME'); ?></a>
		</li>
		<?php
		$usersConfig = JComponentHelper::getParams('com_users');
		if ($usersConfig->get('allowUserRegistration')) : ?>
		<?php endif; ?>
	</ul>
	<button type="submit" class="validate" style="margin-top: 74px;
border: none;float: right;
cursor: pointer;"><img src="images/iniciaaho.png" /></button>
	<?php if ($params->get('posttext')): ?>
		<div class="posttext">
		<p><?php echo $params->get('posttext'); ?></p>
		</div>
		<?php endif; ?>
	</div>
	
</form>
<?php endif; ?>
