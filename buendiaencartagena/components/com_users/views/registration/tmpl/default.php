﻿<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.noframes');
?>
<style>
#tituloins
{
width:422px;
height:44px;
}
</style>

<div id="tituloins"><img src="images/tituloins.png" /></div>
<div style="background:#edeeef; color:#492f26; border: 2px solid #FCFCFC; height: 244px; font-size:13px; padding: 19px;margin-top: -2px;
margin-left: 5px;box-shadow: 0 0 13px #CDCBCB; -webkit-box-shadow: 0 0 13px #CDCBCB; -moz-box-shadow: 0 0 13px #CDCBCB; -ms-box-shadow: 0 0 13px #CDCBCB; -o-box-shadow: 0 0 13px #CDCBCB;">
<div style="" class="registration<?php echo $this->pageclass_sfx?>">
<?php if ($this->params->get('show_page_heading')) : ?>
	<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
<?php endif; ?>

	<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate">
<?php foreach ($this->form->getFieldsets() as $fieldset): // Iterate through the form fieldsets and display each one.?>
	<?php $fields = $this->form->getFieldset($fieldset->name);?>
	<?php if (count($fields)):?>
		
		<?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.
		?>
			
		<?php endif;?>
			<dl>
		<?php foreach($fields as $field):// Iterate through the fields in the set and display them.?>
			<?php if ($field->hidden):// If the field is hidden, just display the input.?>
				<?php echo $field->input;?>
			<?php else:?>
				<dt>
					<?php echo $field->label; ?>
					<?php if (!$field->required && $field->type!='Spacer'): ?>
						<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
					<?php endif; ?>
				</dt>
				<dd><?php echo ($field->type!='Spacer') ? $field->input : "&#160;"; ?></dd>
			<?php endif;?>
		<?php endforeach;?>
			</dl>
		
	<?php endif;?>
<?php endforeach;?>
		<div>
			<button type="submit" class="validate" style="margin-top: -10px;
border: none;float: right;
cursor: pointer;"><img src="images/inscribeteaho.png" /></button>
			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="registration.register" />
			<?php echo JHtml::_('form.token');?>
		</div>
	</form>
</div>
</div>
<br><br><br><br>