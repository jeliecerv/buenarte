<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

?>


<br>
	<dl>
		<dt>
			<b><?php echo JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?></b><?php echo $this->data->name; ?>
		</dt>
		<dd>
			
		</dd>
		<dt>
			<b><?php echo JText::_('COM_USERS_PROFILE_USERNAME_LABEL'); ?></b><?php echo htmlspecialchars($this->data->username); ?>
		</dt>
		<dd>
			
		</dd>
		<dt>
			<b><?php echo JText::_('COM_USERS_PROFILE_REGISTERED_DATE_LABEL'); ?></b><?php echo JHtml::_('date', $this->data->registerDate); ?>
		</dt>
		<dd>
			
		</dd>
		<dt>
			<b><?php echo JText::_('COM_USERS_PROFILE_LAST_VISITED_DATE_LABEL'); ?></b>
		

		<?php if ($this->data->lastvisitDate != '0000-00-00 00:00:00'){?>
			<dd>
				<?php echo JHtml::_('date', $this->data->lastvisitDate); ?>
			</dd>
		<?php }
		else {?>
			<dd>
				<?php echo JText::_('COM_USERS_PROFILE_NEVER_VISITED'); ?>
			</dd>
		<?php } ?>
		</dt>
	</dl>
