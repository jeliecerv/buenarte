<?php defined('_JEXEC') or die('Direct Access to this location is not allowed.'); ?>
<style>

input#single_imgtitle
{
background:none;
height: 23px;
border: 2px #aeaeae solid;
border-radius: 6px;
width: 270px;
}

textarea#single_imgtext
{
background: none;
height: 102px;
border: 2px #aeaeae solid;
border-radius: 6px;
width: 270px;
}

.formelm
{
font-family:verdana;
  color:##492f26;
}


</style>
<div style="width: 321px; margin: 0 auto;"><img src="images/cargafot.png"/></div> 
<form action="<?php echo JRoute::_('index.php?type=single'); ?>" method="post" name="adminForm" id="SingleUploadForm" enctype="multipart/form-data" class="form-validate" onsubmit="if(this.task.value == 'upload.upload' && !document.formvalidator.isValid(document.id('SingleUploadForm'))){alert('<?php echo JText::_('JGLOBAL_VALIDATION_FORM_FAILED', true); ?>');return false;} return joomOnSubmit();">
  <div>
  <div class="formelm" style="width: 370px;margin: 5px 0;font-family: verdana;color: #492f26;">
      <?php echo $this->single_form->getLabel('imgauthor'); ?>
      <b><?php echo JHTML::_('joomgallery.displayname', $this->_user->get('id'), 'upload'); ?></b>
    </div>
    <div class="formelm">
     <input type="hidden" id="single_catid" name="catid" value="8">
    </div>
	<input type = "hidden" name="published" id="single-published" value="1" >
      <?php if(!$this->_config->get('jg_useruseorigfilename')): ?>
    <div class="formelm" style="width:279px;">
      <?php echo $this->single_form->getLabel('imgtitle'); ?>
      <?php echo $this->single_form->getInput('imgtitle'); ?>
    </div>
      <?php endif;
            if(!$this->_config->get('jg_useruseorigfilename') && $this->_config->get('jg_useruploadnumber')): ?>
    <div class="formelm" style="width: 281px;">
      <?php echo $this->single_form->getLabel('filecounter'); ?>
      <?php echo $this->single_form->getInput('filecounter'); ?>
    </div>
      <?php endif; ?>
    <div class="formelm" style="width: 281px;">
      <?php echo $this->single_form->getLabel('imgtext'); ?>
      <?php echo $this->single_form->getInput('imgtext'); ?>
    </div>
    
    
    <?php /*<div class="formelm">
      <?php echo $this->single_form->getLabel('access'); ?>
      <?php echo $this->single_form->getInput('access'); ?>
    </div> */ ?>
    <div class="formelm">
      <?php echo $this->single_form->getLabel('arrscreenshot'); ?>
      <?php echo $this->single_form->getInput('arrscreenshot'); ?>
    </div>
      <?php if($this->_config->get('jg_delete_original_user') == 2): ?>
    <div class="formelm">
      <?php echo $this->single_form->getLabel('original_delete'); ?>
      <?php echo $this->single_form->getInput('original_delete'); ?>
    </div>
      <?php endif;
            if($this->_config->get('jg_special_gif_upload') == 1): ?>
			
      <?php endif;
            if($this->_config->get('jg_redirect_after_upload')): ?>
    <div style="visibility:hidden;" class="formelm">
      <?php echo $this->single_form->getLabel('debug'); ?>
      <?php echo $this->single_form->getInput('debug'); ?>
    </div>
      <?php endif; ?>
    <div class="formelm">
      <label for="button"></label>
      <button type="submit" class="button" style="border:none;cursor:pointer;background:none;"><img src="images/botonsub.png"></button>
      
    </div>
    <input type="hidden" name="task" value="upload.upload" />
  </div>
</form>