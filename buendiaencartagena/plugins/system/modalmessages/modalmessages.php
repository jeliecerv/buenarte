<?php
/*------------------------------------------------------------------------
# Modal syslem messages
# ------------------------------------------------------------------------
# Dmitry Chernov
# Copyright (C) 2011 Provitiligo.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.provitiligo.com
# Support:  provitiligo@gmail.com
-------------------------------------------------------------------------*/

// no direct access

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

$lang = JFactory::getLanguage();
$lang->load('plg_system_modalmessages');

class plgSystemModalmessages extends JPlugin
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
	}

	function onAfterInitialise()
	{
		if(JFactory::getApplication()->isAdmin()) return true;

		$document =& JFactory::getDocument();
		$document->addStyleSheet(JURI::root().'plugins/system/modalmessages/assets/css/style.css');

		if ($this->params->def('loadjquery'))
		{
			$document->addScript("plugins/system/modalmessages/assets/js/jquery-1.7.1.min.js");
		}

		$script = "	jQuery.noConflict();
					
					jQuery(document).ready(function()
					{
						jQuery('#modal-messages').css('margin-top', ((jQuery(window).height() - jQuery('#modal-messages').outerHeight())/2) + jQuery(window).scrollTop() + 'px');
						jQuery('#modal-messages').css('margin-left', ((jQuery(window).width() - jQuery('#modal-messages').outerWidth())/2) + jQuery(window).scrollLeft() + 'px');
						jQuery('#modal-messages').show();";

		if ($this->params->def('dimback'))
		{
    		$script .= "jQuery('#messages-overlay').show();

    					jQuery('#messages-close-button').click(function()
    					{
							jQuery('#messages-overlay').hide();
							jQuery('#modal-messages').hide();
    					});

    					jQuery('#messages-overlay').click(function()
    					{
							jQuery('#messages-overlay').hide();
							jQuery('#modal-messages').hide();
    					});";
    	}
    	else
    	{
        	$script .= "jQuery('#messages-close-button').click(function()
    					{
							jQuery('#modal-messages').hide();
    					});

    					jQuery('#messages-overlay').click(function()
    					{
							jQuery('#modal-messages').hide();
    					});";
    	}

		$script .= "});";

		$document->addScriptDeclaration($script);
	}

	function onAfterRender()
	{
		if(JFactory::getApplication()->isAdmin()) return true;

		$messages = JFactory::getApplication()->getMessageQueue();
		$content = "";

		if($messages)
		{
			foreach ($messages as $message)
			{
				if (($message['type'] != "message") && ($message['type'] != "notice") && ($message['type'] != "error"))
				{
					$type = "message";
				}
				else
				{
					$type = $message['type'];
				}

				$content .= "<span class='messages-".$type."'>".$message['message']."</span>";
			}

        	$output = JResponse::getBody();
        	$pattern = '/<\/body>/';
							
	        $replacement = '<div id="modal-messages" style="display: none">
								<div id="messages-header" class="'.$type.'">
									<div id="messages-title">'.JText::_('PLG_SYSTEM_MODALMESSAGES_TITLE').'</div>
									<div id="messages-close">
										<a id="messages-close-button" href="javascript:void(0)" title="'.JText::_('PLG_SYSTEM_MODALMESSAGES_CLOSE').'">
											<img src="'.JURI::root().'plugins/system/modalmessages/images/close.png">
										</a>
									</div>
								</div>
								<div id="messages-main">
									<p>'.$content.'</p>
								</div>
								<div id="messages-footer">'.($this->params->def("branding") ? $this->params->def("branding") : "").'</div>
							</div>
							<div id="messages-overlay" style="display: none"></div></body>';
        	
        	$output = preg_replace($pattern, $replacement, $output);
	        JResponse::setBody($output);
	        return true;
		}
	}
}

?>