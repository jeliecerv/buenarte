<?php
/*------------------------------------------------------------------------
# Social Authorization
# ------------------------------------------------------------------------
# Dmitry Chernov
# Copyright (C) 2012 Provitiligo.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.provitiligo.com
# Support:  provitiligo@gmail.com
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldAsset extends JFormField
{
    protected $type = 'Asset';

    protected function getInput()
    {
		$checkJqueryLoaded = false;
		$document	= &JFactory::getDocument();
		$header = $document->getHeadData();

		foreach($header['scripts'] as $scriptName => $scriptData)
		{
			if(substr_count($scriptName,'jquery.min.js')){
				$checkJqueryLoaded = true;
			}
		}

		if(!$checkJqueryLoaded)
		{
			$document->addScript(JURI::root().$this->element['path'].'js/jquery.min.js');
		}
		
		$document->addScript(JURI::root().$this->element['path'].'js/chosen.jquery.min.js');		
        $document->addScript(JURI::root().$this->element['path'].'js/main.js');
        
		$document->addStyleSheet(JURI::root().$this->element['path'].'css/main.css');
        $document->addStyleSheet(JURI::root().$this->element['path'].'css/chosen.css');        
                
        return null;
    }
}

?>