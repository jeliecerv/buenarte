/*------------------------------------------------------------------------
# Social Authorization
# ------------------------------------------------------------------------
# Dmitry Chernov
# Copyright (C) 2012 Provitiligo.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.provitiligo.com
# Support:  provitiligo@gmail.com
-------------------------------------------------------------------------*/

jQuery.noConflict();

window.addEvent("domready", function()
{
	$$("#jform_params_asset-lbl").getParent().destroy();
	
	$$('.switch').each(function(el)
	{
		var options = el.getElements('option');		
		
		if(options.length == 2)
		{
			el.setStyle('display', 'none');

			var value = new Array();
			value[0] = options[0].value;
			value[1] = options[1].value;
			
			var text = new Array();
			text[0] = options[0].text.replace(" ", "-").toLowerCase().trim();
			text[1] = options[1].text.replace(" ", "-").toLowerCase().trim();
				
			var switchClass = (el.value == value[0]) ? text[0] : text[1];
			var switcher = new Element('div', {'class' : 'switcher-' + switchClass});

			switcher.inject(el, 'after');
			
			switcher.addEvent("click", function()
			{
				if(el.value == value[1])
				{
					switcher.setProperty('class', 'switcher-' + text[0]);
					el.value = value[0];
				}
				else
				{
					switcher.setProperty('class', 'switcher-' + text[1]);
					el.value = value[1];
				}
			});
		}
	});

	jQuery(".pane-sliders select").each(function()
	{
		if(jQuery(this).is(":visible"))
		{
			jQuery(this).css("width", parseInt(jQuery(this).width()) + 20);
			jQuery(this).css("min-width", "170px");
			jQuery(this).chosen();
		};
		
		jQuery('.chzn-search').hide();
	});

	jQuery(".chzn-container").click(function()
	{
		jQuery(".panel .pane-slider,.panel .panelform").css("overflow", "visible");	
	});
	
	jQuery(".panel .title").click(function()
	{
		jQuery(".panel .pane-slider,.panel .panelform").css("overflow", "hidden");		
	});
});