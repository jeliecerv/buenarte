<?php
// no direct access
defined('_JEXEC') or die;

abstract class modSliderGalleryHelper
{
	public static function sliderGallery(&$params)
	{
		$db  = JFactory::getDbo();
		$imgs_gallery = array();
		$query  = $db->getQuery(true);

		$option = JRequest::getCmd('option');
		$view  = JRequest::getCmd('view');

		$temp  = JRequest::getString('id');
		$temp  = explode(':', $temp);
		$id  = $temp[0];

		$categoryId = $params->get('gallery_category_id');

  		if ($option == 'com_content' && $view == 'article' && $id) 
		{  
			$query->select('a.catid');
			$query->select('a.imgtitle');
			$query->select('a.imgfilename');
			$query->select('a.imgthumbname');
			$query->select('a.owner');
			$query->select('b.name');
			$query->select('b.username');
			$query->select('c.name AS name_category');
			            
			$query->from('#__joomgallery AS a');
			$query->from('#__users AS b');
			$query->from('#__joomgallery_catg AS c');

			$query->where('a.catid = ' . $categoryId);
			$query->where('a.published = 1');
			$query->where('a.approved = 1');
			$query->where('a.owner = b.id');
			$query->where('a.catid = c.cid');
			
			$db->setQuery($query);
			$imgs_gallery = $db->loadObjectList();

			if (count($imgs_gallery))
			{
				foreach ($imgs_gallery as $row)
				{
					$path = "";
					switch ($categoryId) {
						case 7: //Profesional
							$path = 'profesional_7/'.$row->imgfilename;
							break;
						case 4: //Niños
							$path = 'ninos_4/'.$row->imgfilename;
							break;
						case 2: //Amateur
							$path = 'amateur_2/'.$row->imgfilename;
							break;
						case 9: //Moviles
							$path = 'moviles_9/'.$row->imgfilename;
							break;
						
						default:
							# code...
							break;
					}
					$row->path = $path;

				}
			}
		}
		return $imgs_gallery;
	}

	public static function getNameCategory(&$params)
	{
		$db  = JFactory::getDbo();
		$query  = $db->getQuery(true);
		
		$categoryId = $params->get('gallery_category_id');

		$query->select('c.name AS name_category');
		
		$query->from('#__joomgallery_catg AS c');

		$query->where('c.cid = ' . $categoryId);
		
		$db->setQuery($query);
		$category = $db->loadObject();

		$class = "";
		$gallery_path = "";
		switch ($categoryId) {
			case 7: //Profesional
				$class = "profesional_color";
				$gallery_path = 'galería/profesional.html';
				break;
			case 4: //Niños
				$class = "ninos_color";
				$gallery_path = 'galería/ninos.html';
				break;
			case 2: //Amateur
				$class = "amateur_color";
				$gallery_path = 'galería/amateur.html';
				break;
			case 9: //Moviles
				$class = "moviles_color";
				$gallery_path = 'galería/moviles.html';
				break;
			
			default:
				# code...
				break;
		}
		$category->class = $class;
		$category->gallery_path = $gallery_path;

		return $category;
	}
}
