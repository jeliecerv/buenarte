<?php
// no direct access
defined('_JEXEC') or die;
?>
<link href="templates/buenarte_interna/css/flexslider.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="templates/buenarte_interna/script/jquery.flexslider-min.js"></script>
<link href="templates/buenarte_interna/script/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
<script src="templates/buenarte_interna/script/fancybox/jquery.fancybox.pack.js"></script>

<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1452716098279555',
      status     : true,
      xfbml      : true
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/es_LA/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    $(window).load(function() {
	  // The slider being synced must be initialized first
	  $('#carousel').flexslider({
	    animation: "slide",
	    controlNav: false,
	    animationLoop: true,
	    slideshow: false,
	    itemWidth: 150,
	    smoothHeight: true,
	    itemMargin: 0,
	    asNavFor: '#slider'
	  });
	   
	  $('#slider').flexslider({
	    animation: "slide",
	    controlNav: false,
	    animationLoop: true,
	    slideshow: false,
	    smoothHeight: true,
	    sync: "#carousel",
	    animationSpeed: 300,
	    move: 0,
	    keyboard: true,
	    start: function(slider) {
	    	$('p[id*=autor-]').hide();
			$('p[id*=titulo-]').hide();
			$('p[id*=face_share-]').hide();
			$('em[id*=sep-]').hide();
			$('em[id*=sep2-]').hide();
			$('p[zoom_gallery-]').hide();

	    	currentSlide = slider.currentSlide;
			count = slider.count;
			$('#currentSlider').text((currentSlide + 1) + '/' + count);
			
			$('#autor-'+currentSlide).show();
			$('#titulo-'+currentSlide).show();
			$('#face_share-'+currentSlide).show();
			$('#sep-'+currentSlide).show();
			$('#sep2-'+currentSlide).show();
			$('#zoom_gallery-'+currentSlide).show();
	    },
	    after: function(slider) {
			$('p[id*=autor-]').hide();
			$('p[id*=titulo-]').hide();
			$('p[id*=face_share-]').hide();
			$('em[id*=sep-]').hide();
			$('em[id*=sep2-]').hide();
			$('p[id*=zoom_gallery-]').hide();

			currentSlide = slider.currentSlide;
			count = slider.count;
			$('#currentSlider').text((currentSlide + 1) + '/' + count);

			$('#autor-'+currentSlide).show();
			$('#titulo-'+currentSlide).show();
			$('#face_share-'+currentSlide).show();
			$('#sep-'+currentSlide).show();
			$('#sep2-'+currentSlide).show();
			$('#zoom_gallery-'+currentSlide).show();
		}
	  });

	  	$('#slider').flexslider("play");

	  	$('.zoom_gallery_icon').click(function() {
	  		$('#slider').flexslider("pause");
	  		var imgZoom = $(this).attr('refimg');
	  		var originalImg = $(this).attr('originalimg');
	  		$(imgZoom).parent('a').attr('href', originalImg);
	  		$(imgZoom).parent('a').fancybox({
				openEffect	: 'elastic',
				closeEffect	: 'elastic'
			});
			$(imgZoom).parent('a').click();
	  	});

	  	$('.fancybox').click(function(ev) {
	  		if($(this).attr('href') == '#') {
	  			ev.preventDefault();
	  			return false;
	  		}else {
	  			return true;
	  		}
	  	});

	});
</script>
<div id="cont_gallery" class="<?php echo $moduleclass_sfx; ?>">
	<h1 class="<?php echo $category->class ?>" >Categoria: <?php echo $category->name_category; ?></h1>
	<div id="slider" class="flexslider">
		<ul class="slides">
			<?php 
			$count = 0;
			if (count($list)) {
				foreach ($list as $item) : 
			?>
		    	<li>
		    		<a class="fancybox" href="#" title="<?php echo $item->imgtitle ?>">
			      		<img  id="img_gallery-<?php echo $count++; ?>" class="zoom_gallery" src="images/joomgallery/details/<?php echo $item->path; ?>" >
			      	</a>
			    </li>
		    <?php 
		    	endforeach; 
		    }else {
		    ?>
		    	<li>
		    		<a class="fancybox" href="#" title="Buenarte muestra">
			      		<img id="img_gallery-0" class="zoom_gallery" src="images/galeria/img-temporal.jpg" >
			      	</a>
			    </li>
		    <?php } ?>
	    </ul>
	</div>
	<div id="info_img">
		<p id="currentSlider" ></p>
		<em class="separador"></em>
		<?php 
		$count = 0;
		if (count($list)) {
			foreach ($list as $item) : 
		?>
	    	<p style="display:none;" id="autor-<?php echo $count; ?>" class="autor_gallery">
	    		<strong>Autor:</strong> <?php echo $item->username ?>
	    	</p>
	    	<p style="display:none;" id="titulo-<?php echo $count; ?>" class="titulo_gallery">
	    		<strong>Titulo:</strong> <?php echo $item->imgtitle ?>
	    	</p>
	    	<em style="display:none;" class="separador" id="sep-<?php echo $count; ?>" ></em>
	    	<p  style="display:none;" id="face_share-<?php echo $count; ?>" class="face_gallery">
	    		<img src="images/galeria/face.png" alt="face"> 
	    		<a href="https://www.facebook.com/sharer.php?u=<?php echo urlencode( JURI::base().$category->gallery_path ) ?>&t=<?php echo urlencode($item->imgtitle) ?>/" target="_blank">
				  Compartir
				</a>
	    	</p>
	    	<em style="display:none;" class="separador" id="sep2-<?php echo $count; ?>"></em>
			<p refimg="#img_gallery-<?php echo $count; ?>" originalimg="<?php echo JURI::base() ?>images/joomgallery/originals/<?php echo $item->path; ?>" style="display:none;" class="zoom_gallery_icon" id="zoom_gallery-<?php echo $count++; ?>"><img src="images/galeria/zoom.png" alt="zoom"></p>
		<?php 
			endforeach; 
		}else {
		?>
			<p style="display:none;" id="autor-0" class="autor_gallery">
	    		<strong>Autor:</strong> Buenarte
	    	</p>
	    	<p style="display:none;" id="titulo-0" class="titulo_gallery">
	    		<strong>Titulo:</strong> Buenarte muestra
	    	</p>
	    	<em style="display:none;" class="separador" id="sep-0" ></em>
	    	<p  style="display:none;" id="face_share-0" class="face_gallery">
	    		<img src="images/galeria/face.png" alt="face"> 
	    		<a href="https://www.facebook.com/sharer.php?u=<?php echo urlencode( JURI::base().$category->gallery_path ) ?>&t=<?php echo urlencode('Buenarte muestra') ?>/" target="_blank">
				  Compartir
				</a>
	    	</p>
	    	<em style="display:none;" class="separador" id="sep2-0"></em>
			<p refimg="#img_gallery-0" originalimg="<?php echo JURI::base() ?>images/galeria/img-temporal.jpg" style="display:none;" class="zoom_gallery_icon" id="zoom_gallery-0"><img src="images/galeria/zoom.png" alt="zoom"></p>
		<?php } ?>
	</div>
	<div id="carousel" class="flexslider">
	  	<ul class="slides">
		    <?php 
		    if (count($list)) {
		    	foreach ($list as $item) : 
    		?>
		    	<li>
			      	<img src="images/joomgallery/thumbnails/<?php echo $item->path; ?>" >
			    </li>
		    <?php 
		    	endforeach; 
		    }else {
		    ?>
				<li>
			      	<img src="images/galeria/img-temporal.jpg" >
			    </li>
		    <?php } ?>
	    </ul>
	</div>
</div>