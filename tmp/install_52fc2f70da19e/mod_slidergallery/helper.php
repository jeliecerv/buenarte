<?php
// no direct access
defined('_JEXEC') or die;

abstract class modSliderGalleryHelper
{
	public static function sliderGallery(&$params)
	{
		$db  = JFactory::getDbo();
		$imgs_gallery = array();
		$query  = $db->getQuery(true);

		$option = JRequest::getCmd('option');
		$view  = JRequest::getCmd('view');

		$temp  = JRequest::getString('id');
		$temp  = explode(':', $temp);
		$id  = $temp[0];

  		if ($option == 'com_content' && $view == 'article' && $id) 
		{  
			$query->select('a.cat_id');
			$query->select('a.imgtitle');
			$query->select('a.imgfilename');
			$query->select('a.imgthumbname');
			$query->select('a.owner');
			$query->select('b.name');
			$query->select('b.username');
			            
			$query->from('#__joomgallery AS a');
			$query->from('#__users AS b');

			$query->where('a.cat_id = ' . 7);
			$query->where('a.published = 1');
			$query->where('a.approved = 1');
			$query->where('a.owner = b.id');
			
			$db->setQuery($query);
			$imgs_gallery = $db->loadObjectList();

			if (count($imgs_gallery))
			{
				foreach ($imgs_gallery as $row)
				{
					print_r($row);
				}
			}
		}

		return $imgs_gallery;
	}
}
