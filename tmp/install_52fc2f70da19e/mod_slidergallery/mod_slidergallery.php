<?php
<?php
// No permitir el acceso directo al archivo
defined('_JEXEC') or die;

// Incluye las funciones sólo una vez
require_once dirname(__FILE__).'/helper.php';

// Invocar el método apropiado
$cacheparams = new stdClass;
$cacheparams->cachemode = 'safeuri';
$cacheparams->class = 'modSliderGalleryHelper';
$cacheparams->method = 'sliderGallery';
$cacheparams->methodparams = $params;
$cacheparams->modeparams = array('id'=>'int', 'Itemid'=>'int');

$list = JModuleHelper::moduleCache ($module, $params, $cacheparams);

if (!count($list)) {
 return;
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

// Incluir la plantilla que mostrará los datos recogidos
require JModuleHelper::getLayoutPath('mod_slidergallery', $params->get('layout', 'default'));