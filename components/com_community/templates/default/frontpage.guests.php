<?php
/**
 * @package		JomSocial
 * @subpackage 	Template 
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 * 
 */
defined('_JEXEC') or die();
?>
<img src="images/participa/titulo-participa.jpg" alt="">
<div class="greybox">
	<div>
		<div>
			<div class="cFrontpageSignup">
				
				<!-- Start the Intro text -->
				<div class="cFrontPageLeft">
					<div class="introduction">
					<!--style>
					.personn{font-size: 27px !important;
font-family: fertigo_proregular !important;
color: #3CC;
background: url(/images/li2.png) no-repeat 0px 17px;
height: 33px;}

@font-face {
    font-family: 'fertigo_proregular';
    src: url('css/fertigo_pro_regular-webfont.eot');
    src: url('css/fertigo_pro_regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('css/fertigo_pro_regular-webfont.woff') format('woff'),
         url('css/fertigo_pro_regular-webfont.ttf') format('truetype'),
         url('css/fertigo_pro_regular-webfont.svg#fertigo_proregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
					</style>
						<h1 class="personn"><?php echo JText::_('COM_COMMUNITY_GET_CONNECTED_TITLE'); ?></h1>
						<ul id="featurelist">
						    <li><img src="images/uno.png"><?php echo JText::_('COM_COMMUNITY_CONNECT_AND_EXPAND'); ?></li>
						    <li><img src="images/dos.png"><?php echo JText::_('COM_COMMUNITY_VIEW_PROFILES_AND_ADD_FRIEND'); ?></li>
						    <li><img src="images/tres.png"><?php echo JText::_('COM_COMMUNITY_SHARE_PHOTOS_AND_VIDEOS'); ?></li>
						    <li><img src="images/cuatro.png"><?php echo JText::_('COM_COMMUNITY_GROUPS_INVOLVE'); ?></li>
						</ul>
						<?php if ($usersConfig->get('allowUserRegistration')) : ?>
							<div class="joinbutton">
								<a id="joinButton" href="<?php echo CRoute::_( 'index.php?option=com_community&view=register' , false ); ?>" title="<?php echo JText::_('COM_COMMUNITY_JOIN_US_NOW'); ?>">
									<?php echo JText::_('COM_COMMUNITY_JOIN_US_NOW'); ?>
								</a>
							</div>
						<?php endif; ?>-->

						<h1>¡Recuerda!</h1>

						<p>Para subir tus fotografías, cada una de ellas no debe superar los dos Megabytes de peso. Además asegúrate de que estén en formato .jpg o .png.</p>

						<p>Para optimización de fotografías (bajar el peso) te recomendamos hacerlo a través de la página web <a target="_blanck" href="http://www.webresizer.com/resizer/">http://www.webresizer.com/resizer/</a>.</p>

						<p>Si tienes dificultad para subir tus fotografías o cualquier otro inconveniente dentro del sitio web, puedes contactarnos a través del siguiente correo electrónico: <a href="mailto:soportebuenarte@gmail.com">soportebuenarte@gmail.com</a>.</p>

						<p>Ten en cuenta que a partir del domingo 2 de marzo de 2014 a las 12:00 del medio día, la opción de subir fotografías quedará deshabilitada, <strong class="blank">¡así que apresúrate!.</strong></p>

					</div>
				</div>	
				<!-- End Intro text -->
								
				<!-- Start the Login Form -->
				<div class="cFrontPageRight">
					<div class="loginform">
						<img src="images/participa/ingresa-datos.png" alt="">
						<p>Ingresa los datos que te fueron asignados al momento de la inscripción:</p>
						<form action="<?php echo CRoute::getURI();?>" method="post" name="login" id="form-login" >
							<!--<h2><?php echo JText::_('COM_COMMUNITY_MEMBER_LOGIN'); ?></h2>-->
							<label>
								<!--?php echo JText::_('COM_COMMUNITY_USERNAME'); ?><br />-->
								<img id="usr_img" src="images/participa/usuario.jpg" alt="">
								<input type="text" class="inputbox frontlogin" name="username" id="username" />
							</label>

							<label>
								<!--<?php echo JText::_('COM_COMMUNITY_PASSWORD'); ?><br />-->
								<img id="cod_img" src="images/participa/codigo.jpg" alt="">
								<input type="password" class="inputbox frontlogin" name="<?php echo COM_USER_PASSWORD_INPUT;?>" id="password" />
							</label>

							<!--<?php if(JPluginHelper::isEnabled('system', 'remember')) : ?>
								<label for="remember">
									<input type="checkbox" alt="<?php echo JText::_('COM_COMMUNITY_REMEMBER_MY_DETAILS'); ?>" value="yes" id="remember" name="remember"/>
									<?php echo JText::_('COM_COMMUNITY_REMEMBER_MY_DETAILS'); ?>
								</label>
							<?php endif; ?>-->

							<div style="text-align: center; padding: 10px 0 5px;">
								<input type="submit" value="<?php echo JText::_('COM_COMMUNITY_LOGIN_BUTTON');?>" name="submit" id="submit" class="button" />
								<input type="hidden" name="option" value="<?php echo COM_USER_NAME;?>" />
								<input type="hidden" name="task" value="<?php echo COM_USER_TAKS_LOGIN;?>" />
								<input type="hidden" name="return" value="<?php echo $return; ?>" />
								<?php echo JHTML::_( 'form.token' ); ?>
							</div>

							<!--<span>
								<a href="<?php echo CRoute::_( 'index.php?option='.COM_USER_NAME.'&view=reset' ); ?>" class="login-forgot-password"><span><?php echo JText::_('COM_COMMUNITY_FORGOT_YOUR'). ' '. JText::_('COM_COMMUNITY_PASSWORD').'?'; ?></span></a><br />
								<a href="<?php echo CRoute::_( 'index.php?option='.COM_USER_NAME.'&view=remind' ); ?>" class="login-forgot-username"><span><?php echo JText::_('COM_COMMUNITY_FORGOT_YOUR'). ' '. JText::_('COM_COMMUNITY_USERNAME').'?'; ?></span></a>
							</span>
						
							<?php if ($useractivation) { ?>
								<br />
								<a href="<?php echo CRoute::_( 'index.php?option=com_community&view=register&task=activation' ); ?>" class="login-forgot-username">
									<span><?php echo JText::_('COM_COMMUNITY_RESEND_ACTIVATION_CODE'); ?></span>
								</a>
							<?php } ?>-->
						</form>
						
						<?php echo $fbHtml;?>
				    
					</div>
				</div>
				<!-- End the Login form -->
				
				<div class="jsClr"></div>
	    </div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		var $msj_error = $('#system-message-container');
		$('#system-message-container').remove();
		$('.greybox').prepend($msj_error);
		var count_child = $msj_error.children().length;
		if(count_child) {
			$msj_error.show();
		}else{
			$msj_error.hide();
		}
	});
</script>