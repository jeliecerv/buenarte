<?php
/**
 * @package		JomSocial
 * @subpackage 	Template 
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 * 
 */
defined('_JEXEC') or die();
?>
<img src="images/participa/titulo-participa.jpg" alt="">
<div class="denied-box">
<!--<h3 style="margin: 0;"><?php echo JText::_('COM_COMMUNITY_MEMBER_LOGIN');?></h3>
<?php echo JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING');?>-->
	<div class="introduction">
		<h1>¡Recuerda!</h1>

		<p>Para subir tus fotografías, cada una de ellas no debe superar los dos Megabytes de peso. Además asegúrate de que estén en formato .jpg o .png.</p>

		<p>Para optimización de fotografías (bajar el peso) te recomendamos hacerlo a través de la página web <a target="_blanck" href="http://www.webresizer.com/resizer/">http://www.webresizer.com/resizer/</a>.</p>

		<p>Si tienes dificultad para subir tus fotografías o cualquier otro inconveniente dentro del sitio web, puedes contactarnos a través del siguiente correo electrónico: <a href="mailto:soportebuenarte@gmail.com">soportebuenarte@gmail.com</a>.</p>

		<p>Ten en cuenta que a partir del domingo 2 de marzo de 2014 a las 12:00 del medio día, la opción de subir fotografías quedará deshabilitada, <strong class="blank">¡así que apresúrate!.</strong></p>
	</div>
	<div class="loginform">
		<img class="img_participa" src="images/participa/ingresa-datos.png" alt="">
		<p>Ingresa los datos que te fueron asignados al momento de la inscripción:</p>
		<form action="<?php echo CRoute::getURI();?>" method="post" name="login" id="form-login" >
		
			<!--<label for="username" style="display:inline; float: left; width: 100px;"><?php echo JText::_('COM_COMMUNITY_USERNAME'); ?></label>-->
			<img id="usr_img" src="images/participa/usuario.jpg" alt="">
			<input type="text" class="inputbox frontlogin" name="username" id="username" />
			
			<div style="clear: left;"></div>		

			<!--<label for="passwd" style="display:inline; float: left; width: 100px;"><?php echo JText::_('COM_COMMUNITY_PASSWORD'); ?></label>-->
			<img id="cod_img" src="images/participa/codigo.jpg" alt="">
			<input type="password" class="inputbox frontlogin" name="<?php echo COM_USER_PASSWORD_INPUT;?>" id="password" />

			<div style="clear: left;"></div>		

			<!--<?php if(JPluginHelper::isEnabled('system', 'remember')) : ?>
			<label for="remember" style="padding: 4px 0 4px 100px;">
				<input type="checkbox" alt="<?php echo JText::_('COM_COMMUNITY_REMEMBER_MY_DETAILS'); ?>" value="yes" id="remember" name="remember"/>
				<?php echo JText::_('COM_COMMUNITY_REMEMBER_MY_DETAILS'); ?>
			</label>
			<?php endif; ?>-->

			<div style="text-align: center; padding: 10px 0 5px;">
				<input type="submit" value="<?php echo JText::_('COM_COMMUNITY_LOGIN_BUTTON');?>" name="submit" id="submit" class="button" />
				<input type="hidden" name="option" value="<?php echo COM_USER_NAME;?>" />
				<input type="hidden" name="task" value="<?php echo COM_USER_TAKS_LOGIN;?>" />
				<input type="hidden" name="return" value="<?php echo $return; ?>" />
				<?php echo JHTML::_( 'form.token' ); ?>
			</div>
			
			<!--<div style="padding: 12px 0 0 100px">
				<a href="<?php echo CRoute::_( 'index.php?option='.COM_USER_NAME.'&view=reset' ); ?>" class="login-forgot-password"><span><?php echo JText::_('COM_COMMUNITY_FORGOT_PASSWORD'); ?></span></a>
				<br/>
				<a href="<?php echo CRoute::_( 'index.php?option='.COM_USER_NAME.'&view=remind' ); ?>" class="login-forgot-username"><span><?php echo JText::_('COM_COMMUNITY_FORGOT_USERNAME'); ?></span></a>
				<br/>
				<a href="<?php echo CRoute::_( 'index.php?option=com_community&view=register' ); ?>"><span><?php echo JText::_('COM_COMMUNITY_CREATE_ACCOUNT'); ?></span></a>
			</div>-->
		</form>
		<?php echo $fbHtml;?>
	</div>
</div>

<script>
	$(document).ready(function() {
		var $msj_error = $('#system-message-container');
		$('#system-message-container').remove();
		$('.denied-box').prepend($msj_error);
		var count_child = $msj_error.children().length;
		if(count_child) {
			$msj_error.show();
		}else{
			$msj_error.hide();
		}
	});
</script>