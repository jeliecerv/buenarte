﻿<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

//JHtml::_('behavior.keepalive');
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.formvalidation');
//JHtml::_('behavior.noframes');
?>
<img src="images/participa/titulo-participa.jpg" alt="">
<script src="templates/buenarte_interna/script/validate/jquery.validate.min.js" type="text/javascript"></script>
<script src="templates/buenarte_interna/script/validate/additional-methods.min.js" type="text/javascript"></script>
<script src="templates/buenarte_interna/script/validate/localization/messages_es.js" type="text/javascript"></script>
<div class="registration<?php echo $this->pageclass_sfx?>">
	<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate">
	<?php foreach ($this->form->getFieldsets() as $fieldset): // Iterate through the form fieldsets and display each one.?>
		<?php $fields = $this->form->getFieldset($fieldset->name);?>
		<?php if (count($fields)):?>
			
			<?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.
			?>
				
			<?php endif;?>
				<dl>
			<?php foreach($fields as $field):// Iterate through the fields in the set and display them.?>
				<?php if ($field->hidden):// If the field is hidden, just display the input.?>
					<?php echo $field->input;?>
				<?php else:?>
					<dt>
						<?php echo $field->label; ?>
						<?php if (!$field->required && $field->type!='Spacer'): ?>
							<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
						<?php endif; ?>
					</dt>
					<dd><?php echo ($field->type!='Spacer') ? $field->input : "&#160;"; ?></dd>
				<?php endif;?>
			<?php endforeach;?>
				</dl>
			
		<?php endif;?>
	<?php endforeach;?>
	<dt>
		<label id="cod_label" for="codigo" class="blue_label" >Codigo:</label>											
	</dt>
	<dd>
		<strong id="codigo_show"></strong>
	</dd>
	<div class="button-register">
		<button type="submit" class="validate"><?php echo JText::_('JREGISTER');?></button>
		<button type="button" id="cod_generate">Generar Código</button>
		<!--<?php echo JText::_('COM_USERS_OR');?>
		<a href="<?php echo JRoute::_('');?>" title="<?php echo JText::_('JCANCEL');?>"><?php echo JText::_('JCANCEL');?></a>-->
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="registration.register" />
		<?php echo JHtml::_('form.token');?>
	</div>
	</form>

	<form action="<?php echo JRoute::_('index.php');?>" method="post" name="communitylogout" id="communitylogout" class="close_session_bottom">
	    <button type="submit" class="logout_btn" id="logout_true" ></button>
	    <input type="hidden" name="option" value="com_users">
	    <input type="hidden" name="task" value="user.logout">
	    <input type="hidden" name="return" value="L2J1ZW5hcnRlL3BlcmZpbC5odG1s">
	    <?php echo JHtml::_('form.token'); ?>
	</form>
</div>
<script>
	$(document).ready(function() {
		var $msj_error = $('#system-message-container');
		$('#system-message-container').remove();
		$('.registration').prepend($msj_error);
		var count_child = $msj_error.children().length;
		if(count_child) {
			$msj_error.show();
		}else{
			$msj_error.hide();
		}

		$('.hasTip').addClass('blue_label');
		$('.star').remove();

		$('#jform_email1').keyup(function() {
			$('#jform_email2').val($(this).val());
		});

		$('#jform_email1').keyup(function() {
			$('#jform_email2').val($(this).val());
		});

		$('#jform_email1').blur(function() {
			$('#jform_email2').val($(this).val());
		});

		$('#cod_generate').click(function() {
			var pass = generar(7);
			$('#codigo_show').text(pass);
			$('#jform_password1').val(pass);
			$('#jform_password2').val(pass);
		});
		var pass = generar(7);
		$('#codigo_show').text(pass);
		$('#jform_password1').val(pass);
		$('#jform_password2').val(pass);

		$('.required').attr('required', '');
		$("#member-registration").validate({
		  
		  rules: {
		    "jform[email1]": {
		      required: true,
		      email: true
		    },
		    "jform[cedula]": {
		      required: true,
		      digits: true
		    },
		    "jform[telefono]": {
		      required: true,
		      digits: true
		    },
		    "jform[categoria]": {
		      selectcheck: true
		    },
		    "jform[password1]": {
		    	required: true
		    }
		  }	

		});
		jQuery.validator.addMethod('selectcheck', function (value) {
	        return (value != '0');
	    }, "La categoría es obligatoria");
	    
	});

	function generar(longitud)
	{
	  var caracteres = "abcdefghijklmnopqrstuvwxyz012346789";
	  var pass = "";
	  for (i=0; i<longitud; i++) pass += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
	  return pass;
	}
</script>