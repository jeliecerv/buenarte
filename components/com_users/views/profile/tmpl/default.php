<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
?>
<div style="background:#edeeef; border: 2px solid #FCFCFC; height: 400px; padding: 19px;
box-shadow: 0 0 13px #CDCBCB; -webkit-box-shadow: 0 0 13px #CDCBCB; -moz-box-shadow: 0 0 13px #CDCBCB; -ms-box-shadow: 0 0 13px #CDCBCB; -o-box-shadow: 0 0 13px #CDCBCB;">
<div style="float:left; width: 295px; height: 410px; font-family: helvetica;
margin-top: 15px; class="profile<?php echo $this->pageclass_sfx?>">
<?php if ($this->params->get('show_page_heading')) : ?>
<h1>
	<?php echo $this->escape($this->params->get('page_heading')); ?>
</h1>
<?php endif; ?>

<?php echo $this->loadTemplate('core'); ?>



<?php echo $this->loadTemplate('custom'); ?>

<a href="index.php?option=com_joomgallery&view=gallery"><div style="height: 35px;
width: 180px;
text-align: center; color:#FFF; background:url(../images/fondo_menu.png); margin: 18px auto;
font-size: 20px;
line-height: 31px;">Ir a votar</div></a>

<?php if (JFactory::getUser()->id == $this->data->id) : ?>
<a style="background:url(images/fondo_menu.png); color:#FFF; height:30px; width:100px; padding: 6px 19px;
margin: 10px 88px;
" href="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id='.(int) $this->data->id);?>">
	<?php echo JText::_('COM_USERS_Edit_Profile'); ?></a>


<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.logout'); ?>" method="post">
		<div>
			<button style="background: none;
border: none;
cursor: pointer;
color: #3CC;
margin-top: 69px; font-size:18px;" type="submit" class="button">Cerrar Sesion</button>
			<input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('logout_redirect_url', $this->form->getValue('return'))); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>

<?php endif; ?>
</div>
<div style="float:left; border-left: 2px solid #3CC;"><img src="images/ima_perf.jpg"></div>
</div>
