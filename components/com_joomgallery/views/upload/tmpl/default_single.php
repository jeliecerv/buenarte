<?php defined('_JEXEC') or die('Direct Access to this location is not allowed.'); ?>
<script src="templates/buenarte_interna/script/validate/jquery.validate.min.js" type="text/javascript"></script>
<script src="templates/buenarte_interna/script/validate/additional-methods.min.js" type="text/javascript"></script>
<script src="templates/buenarte_interna/script/validate/localization/messages_es.js" type="text/javascript"></script>
<form action="<?php echo JRoute::_('index.php?type=single'); ?>" method="post" name="adminForm" id="SingleUploadForm" enctype="multipart/form-data" class="form-validate" onsubmit="if(this.task.value == 'upload.upload' && !document.formvalidator.isValid(document.id('SingleUploadForm'))){alert('<?php echo JText::_('JGLOBAL_VALIDATION_FORM_FAILED', true); ?>');return false;} return joomOnSubmit();">
  <div id="form_upload_img">
    <div class="formelm">
      <h1>¡Hola <?php echo JHTML::_('joomgallery.displayname', $this->_user->get('id'), 'upload'); ?>!</h1>
      <p>Selecciona las fotografías que deseas subir.</p>
    </div>
    <div class="formelm">
      <?php 
      $user = JFactory::getUser();
      ?>
      <input type="hidden" name="catid" value="<?php echo $user->categoria; ?>">
    </div>
    <?php if(!$this->_config->get('jg_useruseorigfilename')): ?>
      <div class="formelm" style="display:none">
        <?php echo $this->single_form->getLabel('imgtitle'); ?>
        <?php echo $this->single_form->getInput('imgtitle'); ?>
      </div>
    <?php endif;
    if(!$this->_config->get('jg_useruseorigfilename') && $this->_config->get('jg_useruploadnumber')): ?>
      <div class="formelm" style="display:none">
        <?php echo $this->single_form->getLabel('filecounter'); ?>
        <?php echo $this->single_form->getInput('filecounter'); ?>
      </div>
    <?php endif; ?>
    <div class="formelm" style="display:none">
      <?php echo $this->single_form->getLabel('imgtext'); ?>
      <?php echo $this->single_form->getInput('imgtext'); ?>
    </div>
    
    <div class="formelm" style="display:none">
      <?php echo $this->single_form->getLabel('published'); ?>
      <?php echo $this->single_form->getInput('published'); ?>
    </div>
    <?php /*<div class="formelm">
      <?php echo $this->single_form->getLabel('access'); ?>
      <?php echo $this->single_form->getInput('access'); ?>
    </div> */ ?>
    <div class="formelm">
      <fieldset class="validate-joomfiles jg-upload-fieldset" id="arrscreenshot">
        <?php for($i=0; $i<$this->remainder; $i++) { ?>
          <div class="fields_file_img">
            <div>
              <label for="foto<?php echo ($i + 1); ?>">Fotografía #<?php echo ($i + 1); ?></label>
              <input type="file" name="arrscreenshot[<?php echo $i; ?>]" id="arrscreenshot<?php echo $i; ?>" value="" class="inputbox validate-joomfiles" size="60">
            </div>
            <div>
              <p>Título de la fotografía:</p>
              <input type="text" name="imgtitle[<?php echo $i; ?>]" id="single_imgtitle<?php echo $i; ?>" value="" class="inputbox required" >
            </div>
          </div>
        <?php } ?>
      </fieldset>
    </div>
    <?php if($this->_config->get('jg_delete_original_user') == 2): ?>
      <div class="formelm" style="display:none">
        <?php echo $this->single_form->getLabel('original_delete'); ?>
        <?php echo $this->single_form->getInput('original_delete'); ?>
      </div>
    <?php endif;
    if($this->_config->get('jg_special_gif_upload') == 1): ?>
      <div class="formelm" style="display:none">
        <?php echo $this->single_form->getLabel('create_special_gif'); ?>
        <?php echo $this->single_form->getInput('create_special_gif'); ?>
      </div>
    <?php endif;
    if($this->_config->get('jg_redirect_after_upload')): ?>
      <div class="formelm" style="display:none">
        <?php echo $this->single_form->getLabel('debug'); ?>
        <?php echo $this->single_form->getInput('debug'); ?>
      </div>
    <?php endif; ?>
    <div>
      <input type="checkbox" name="condition" value="1"><a class="terms" target="_blanck" href="media/autorizacion/TERMINOS_CONCURSO_FOTOGRAFIA_2014_R1.pdf">Acepta términos y condiciones.</a>
      <label for="condition" class="error"></label>
    </div>
    <div class="formelm btn_upload">
      <button type="button" id="logout_fake" class="logout_btn"></button>
      <button type="submit" class="button"><?php echo JText::_('COM_JOOMGALLERY_UPLOAD_UPLOAD'); ?></button>
      <?php if($this->numberofpics > 0) { ?>
        <a class="viewminigaleria" href="<?php echo JRoute::_('index.php?view=userpanel', false); ?>">Ir a la mini galería</a>
      <?php } ?>
    </div>
    <input type="hidden" name="task" value="upload.upload" />
  </div>
</form>
<form action="<?php echo JRoute::_('index.php');?>" method="post" name="communitylogout" id="communitylogout" class="close_session_bottom">
    <button type="submit" class="logout_btn" id="logout_true" style="display:none"></button>
    <input type="hidden" name="option" value="com_users">
    <input type="hidden" name="task" value="user.logout">
    <input type="hidden" name="return" value="L2J1ZW5hcnRlL3BlcmZpbC5odG1s">
    <?php echo JHtml::_('form.token'); ?>
</form>

<script>
  $(document).ready(function() {
    var $msj_error = $('#system-message-container');
    $('#system-message-container').remove();
    $('#upload_cont').prepend($msj_error);
    var count_child = $msj_error.children().length;
    if(count_child) {
      $msj_error.show();
    }else{
      $msj_error.hide();
    }

    $('#single_imgtitle0').change(function() {
      $('#single_imgtitle').val($(this).val());
    });

    $('#logout_fake.logout_btn').click(function() {
      $('#logout_true').click();
    });

    $('input').attr('required', false);
    $('input').removeClass('required');

    $("#SingleUploadForm").validate({
      
      rules: {
        "condition": {
          required: true
        },
        errorPlacement: function (error, element) {
            error.appendTo($(".error"));
        },
      } 

    });

  });
</script>
