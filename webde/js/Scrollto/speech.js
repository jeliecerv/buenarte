
/**variables y sonidos help*/

var v1;
var h0again;
var h1;
var h2;
var h3;
var h4;
var hgroup;
var index;
var arrSounds;
var speechHelp;
var soundSpeech;
var initialized = false;
var noError = true;
var helping = false;
/**variables speech recognition and voice commands*/
var recognitionForNav;
var recognitionForHelp;
var start_timestamp;
var final_transcript;
var voiceSesion = false;

var mensajeDenied = "Webde le recomienda aprobar el uso del micrófono en el navegador<br><br><br><img src='images/jpg/alert.jpg'>";
var noSpeechM = "Reconocimiento de voz desactivado";
var audioCapture= "Captura de audio fallida intente más tarde";
//agregar las palabaras de navegacion correspondientes a mod de arreglos 
//en la segunda columna 
function addPerPageValues() {
    pages[0][1] = new Array(/*pagina*/"next", "naked", "siguiente", "diente"
                                /*slide*/, "anterior", "previous", "reviews",
                                "previews", "preview", "please", "freebies"
                                /*scroll*/, "arriba", "abajo");
    pages[1][1] = new Array("home", "inicio", "hotm");
    pages[2][1] = new Array("servicios", "servicio", "services", "service");
    pages[3][1] = new Array("portafolio", "portfolio");
    pages[4][1] = new Array("equipo", "team", "teen", "tim", "theme", "thin", "dean");
    pages[5][1] = new Array("contacto", "contact", "contac");

}

addPerPageValues();
/*ejemplo corto de la matriz
 	  ______0_______________1_______________    
 0	  |	 "page1" |	["home", "inicio"]		|	
 1	  |	 "page2" |	["sevices", "servicios"]|
 2    |	 "page3" |	["contact", "contacto"] |

 */



function processSpeech(speech) {
    var words = new Array();
    words = speech.split(" ");
    word = speechHelp;
    var word = words[words.length - 1];

    var page;
    switch (word) {
        case "next":
        case "naked":
        case "siguiente":
        case "diente":
            //aqui tiene que ir el nombre de la pagina que tiene scroll tipo 1
            switch (ant)
            {
                case "page2":
                    $("#next1").click();
                    break;
                case "page4":
                    sliderC.prev();
                    cubeElast._slideToItem(sliderC.currentPlace);
                    break;
                case "page5":
                    nextTm();
                    break;

            }
            return;
        case "previous":
        case "anterior":
        case "preview":
        case "previews":
        case "reviews":
        case "please":
        case "freebies":
        case "review":
            //aqui tiene que ir el nombre de la pagina que tiene scroll tipo 1
            if (ant == "page2") {
                $("#prev1").click();
            }
                //pagina con scrooll tipo 2
            else if (ant == "page4") {
                sliderC.prev(); 
                cubeElast._slideToItem(sliderC.currentPlace);
            }
            else if (ant == "page5")
            {
                prevTm();
            }

            return;
        case "arriba":
		case "up":
            
            var pxScroll = $(window).height()/1.7;
            var o = "-="+pxScroll+"px";
            switch(ant)
            {
             case "page5":
             
             $("#page5").scrollTo({ top: o }, 400);
            return;
            case "page3":
             $(".active").scrollTo({ top: o }, 400);
            return;
            case "page4":
             $("#productos").scrollTo({ top: o }, 400);
            return;
            case "page6":
             $("#page6").scrollTo({ top: o }, 400);
            return;
            }
        case "abajo":
		case "down":
            var pxScroll = $(window).height()/1.7;
            var o = "+="+pxScroll+"px";
           switch(ant)
            {
            
             case "page5":
             $("#page5").scrollTo({ top: o }, 400);
            return;
            case "page3":
             $(".active").scrollTo({ top: o }, 400);
            return;
            case "page4":
             $("#productos").scrollTo({ top: o }, 400);
            return;
            case "page6":
             $("#page6").scrollTo({ top: o }, 400);
            return;
            }
		case "tour":
		case "tur":
			cancelVoice();
			startTour();
			return;

    }
    all: for (var i = 1; i < pages.length; i++) {
        for (var j = 0; j < pages[i][1].length; j++) {


            if (word == pages[i][1][j]) {

                page = pages[i][0];
             
                    scrollPage(page);
                
                break all;
            }

        }

    }

}

/**metodos Speech recognition e interactivos*/
function recognitionForNav() {

    recognitionForNav = new webkitSpeechRecognition();
    if (lenguaje == "es") {
        recognitionForNav.lang = "es";
    }
    else {
        recognitionForNav.lang = "en-US";
    }
    recognitionForNav.onresult = function (event) {
        var interim_transcript = '';
        final_transcript = "";
        for (var i = event.resultIndex; i < event.results.length; ++i)
        {
            if (event.results[i].isFinal)
            {
                final_transcript += event.results[i][0].transcript;
            }
            else
            {
                interim_transcript += event.results[i][0].transcript;
            }
        }


        speech = final_transcript;
        processSpeech(speech);
    };


    recognitionForNav.continuous = true;
    recognitionForNav.interimResults = false;
    recognitionForNav.onstart = function () 
    {
        voiceSesion = true;
          $(".letramenu").addClass("colorL");
		$("#pp").addClass("pp");
    };
    recognitionForNav.onerror = function (event) {
        noError = false;
        if (event.error == 'no-speech') {
            cancelVoice();
            alert(noSpeechM);

        }
        if (event.error == 'audio-capture') {
            alert(audioCapture);
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - start_timestamp < 100) {
                alert(mensajeDenied);
            }
            else {
                alert(mensajeDenied);
                voiceSesion = false;
            }
        }
    };
    recognitionForNav.onend = function () {
        if (noError) {
         
            if (voiceSesion) {
                recognitionForNav.start();
            }
        }
        noError = true;
    };
}

function recognitionForHelp() 
{
    recognitionForHelp = new webkitSpeechRecognition();
    if (lenguaje == "es") {
        recognitionForHelp.lang = "es";
    }
    else {
        recognitionForHelp.lang = "en";
    }
    recognitionForHelp.onresult = function (event) {
        var interim_transcript = '';
        final_transcript = "";
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                final_transcript += event.results[i][0].transcript;
            }
            else {
                interim_transcript += event.results[i][0].transcript;
            }
        }
        speechHelp = final_transcript;

    };

    recognitionForHelp.continuous = false;
    recognitionForHelp.interimResults = false;
    recognitionForHelp.onstart = function () { };
    recognitionForHelp.onerror = function (event) 
    {
        cancelHelp();
        noError = false;
        if (event.error == 'no-speech') {
            h0again.play();
            recognitionForHelp.stop();
            recognitionForHelp.start();
            alert(noSpeechM);
        }
        if (event.error == 'audio-capture') {
            alert(audioCapture);
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - start_timestamp < 100) {
                alert(noSpeechM);
            } else {
                alert(noSpeechM);
            }
        }
    };

    recognitionForHelp.onend = function () {
        if (noError) {
            if (acerto()) {
                v1.play();
            }
            else
            {
                h0again.play();
                recognitionForHelp.start();
            }
        }
        noError = true;
    };
}

function acerto() {
    for (var i = 0; i < soundSpeech.length; i++) {
        if (speechHelp == soundSpeech[i]) {
            return true;
        }
    }
    return false
}

function startVoice(event) {
    if (!voiceSesion && !touring&&!helping) {
	
        recognitionForNav.start();
      
        start_timestamp = event.timeStamp;
    }
    else 
    {
        cancelTour();
        cancelVoice();
        cancelHelp();
    }
}
function cancelHelp()
{
	$(".control").addClass("nocontrol");
    $("#help").removeClass("help");
    hGroup.stop();
    if (initialized)
    {
        recognitionForHelp.stop();
    }
     $(".letramenu").removeClass("colorL");
   helping = false;
	
	
}
function cancelVoice()
{
	if(voiceSesion)
	{
        $("#pp").removeClass("pp");;
		voiceSesion = false;
		recognitionForNav.stop();
        $(".letramenu").removeClass("colorL");
	}

}

function playNext() {
    if (index < arrSounds.length) {
        arrSounds[index++].play();
    }
}

function initializeHelpSounds() {

    if (lenguaje == "es") {
        v1 = new buzz.sound("sounds/espanol/help/0bien", { formats: ["ogg", "mp3"] });
        h0again = new buzz.sound("sounds/espanol/help/0deNuevo", { formats: ["ogg", "mp3"] });
        h1 = new buzz.sound("sounds/espanol/help/1menuPrueba", { formats: ["ogg", "mp3"] });
        h2 = new buzz.sound("sounds/espanol/help/2secciones", { formats: ["ogg", "mp3"] });
        h3 = new buzz.sound("sounds/espanol/help/3anterior", { formats: ["ogg", "mp3"] });
        h4 = new buzz.sound("sounds/espanol/help/4finHelp", { formats: ["ogg", "mp3"] });
    }
    else
    {
        v1 = new buzz.sound("sounds/english/help/0bien", { formats: ["ogg", "mp3"] });
        h0again = new buzz.sound("sounds/english/help/0deNuevo", { formats: ["ogg", "mp3"] });
        h1 = new buzz.sound("sounds/english/help/1menuPrueba", { formats: ["ogg", "mp3"] });
        h2 = new buzz.sound("sounds/english/help/2secciones", { formats: ["ogg", "mp3"] });
        h3 = new buzz.sound("sounds/english/help/3anterior", { formats: ["ogg", "mp3"] });
        h4 = new buzz.sound("sounds/english/help/4finHelp", { formats: ["ogg", "mp3"] });
       
    }
    hGroup = new buzz.group([h1, h2, h3, h4]);
    v1.bind("ended", function ()
    {
        processSpeech(speechHelp);
        playNext();
    });
    h1.bind("ended", function () {
        soundSpeech = pages[1][1];

    });
    h2.bind("ended", function ()
    {
        soundSpeech = pages[0][1].slice(0, 4);
    });
    h3.bind("ended", function () {
        soundSpeech = pages[0][1].slice(4, 10);
    });
    h4.bind("ended", function ()
    {
        cancelHelp();
    });
    hGroup.bind("ended",
		function () {

		    if (index < arrSounds.length) {
		        recognitionForHelp.start();

		    }
		});
    arrSounds = hGroup.getSounds();
    initialized = true;


}

function startVHelp()
{
    index = 0;
    if (!initialized)
    {
        initializeHelpSounds();
    }
    if (!helping&&!touring&&!voiceSesion)
    {

        helping = true;
        $(".letramenu").addClass("colorL");
          $("#help").addClass("help");
        playNext();
    }
	else
	{
		$("#help").removeClass("help");
		cancelHelp()
        cancelVoice();
		cancelTour();
	}
}

recognitionForNav();
recognitionForHelp();