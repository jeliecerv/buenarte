var lenguaje = document.documentElement.lang;
var i1;
var i2;
var i3;

//variables scroll
var pages;
var ant="page1";
var speed = 800;
var elast1Init=false;
var elast2Init=false;
var ver_chrome=false;
var cubeSlider;
var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome/') > -1;
var currentMovil;

var cubeElast;
var teamElast;
//variables tour
var arrTSounds;
var t1;
var t2;
var t3;
var t4;
var tInitialized = false;
var indexT = 0;
var actualTSound;
var touring = false;
var opi =false;
var alpha;
var teamM = ["pablo","mm","camilo","ju","alex","pipe","paula","pacho"];
var teamN = new Array();
var currentTm = 0;
var fini;
var page2I, page3I, page4I, page5I, page6I;
var voiceSesion = false;
var helping = false;
//mensaje de pagina
var compatiblidadChrome = "<p>Si esta navegando desde un dispositivo movil <br>no es posible la navegacion por voz, de lo contrario, su navegador no soporta reconocimiento de voz <br> Webde recomienda Google chrome reciente para <br> obtener la maxima experiencia en nuestra pagina.<br><a href='https://www.google.com/chrome/'target='_blank'>click aqui para descargar chrome<a/></p>";


if (is_chrome) 
{
    var posicion = navigator.userAgent.toLowerCase().indexOf('chrome/');
    ver_chrome = navigator.userAgent.toLowerCase().substring(posicion + 7, posicion + 11);
    //Comprobar version
    ver_chrome = parseFloat(ver_chrome);
}


    createArr(6);
    pages[0][0] = "page1";
    pages[1][0] = "page2";
    pages[2][0] = "page3";
    pages[3][0] = "page4";
    pages[4][0] = "page5";
    pages[5][0] = "page6";

function include(file_path) {
    var j = $("<script src="+file_path+"/>");
    $("head").append(j);

}

if (is_chrome && ver_chrome >= 25)
{
    include("js/ScrollTo/speech.js");
}
// crea un array bidimensional
function createArr(numberPages)
{
    pages = new Array();
    for (var i = 0; i < numberPages; i++)
    {
         pages[i] = new Array();
    }
}
//agregar las paginas en la primera columna


function nextTm()
{
        
    
    if (currentTm < teamM.length - 1)
    {
        currentTm++;
    }
    else
    {
        currentTm = 0;
    }
    teamElast._slideToItem(currentTm);
    setTeam(teamM[currentTm]);
}

function prevTm() {


    if (currentTm !=0) {
        currentTm--;
    }
    else {
        currentTm = teamM.length-1;
    }
    teamElast._slideToItem(currentTm);
    setTeam(teamM[currentTm]);
}
// ocultapage4
function hidepage()
{

	$('#page4').css({display: "none"});
}


function onrefresh()
{
	$(".slide").addClass("visible");
}



//muestra la pagina parametro y hace invisibles las demas
function setPage(page,action)
{
	for(var i=0; i<pages.length; i++)
	{
		if(pages[i][0]!=page)
		{
			$("#" + pages[i][0]).css({display:"none"})
		}
	}
	$.scrollTo("#"+page, speed,function()
	{
		$("#page").css({position:"absolute"}); 
		if(action)
		{
			action();
		}                                     
	});
	$('body').css({overflow:"hidden"})
}

//hace visibles todas las paginas para scrolear
function allVisible(ant,page)
{
    $("#" + ant).css("display", "none");
    $("#" + page).css("display", "block");
}
//scrolea hacia la pagina parametro
function scrollPage(page) {
   
 
        allVisible(ant,page);
   
        ant = page;
        subirCortina();
    
}

function bajarCortina(action)
{
    var cortina = $("#cortina");
    cortina.animate({ top: "0" }, 500, function () { action()});
}
function subirCortina()
{
    var cortina = $("#cortina");
    var height = $(window).height() * -1;
    cortina.animate({ top: height }, 500);
}
function getPage2()
{

    bajarCortina(function ()
    {
        if (!page2I) {
            var urlp = "page2" + lenguaje + ".html";
            pAjax(urlp, 2, succes2);
        }
        else {
            scrollPage("page2");
        }

    });
    }

function getPage3() {
    bajarCortina(function () {
        if (!page3I) {
            pAjax("page3" + lenguaje + ".html", 3, succes3);
        }
        else {
            servicios();
        }
    });
   
}

function getPage4() {
    bajarCortina(function () {
        if (!page4I) {
            pAjax("page4" + lenguaje + ".html", 4, succes4);
        }
        else {
            scrollPage("page4");
        }
    });
   
}

function getPage5() {
    bajarCortina(function () {
        if (!page5I) {
            pAjax("page5" + lenguaje + ".html", 5, succes5);
        }
        else {
            scrollPage("page5");
        }
    });
    
}

function getPage6() {
    bajarCortina(function () {
        if (!page6I) {
            pAjax("page6" + lenguaje + ".html", 6, succes6);
        }
        else {
            scrollPage("page6");
        }
    });
    
}



function pAjax(link, page, action)
{
    $.ajax(
            {
                type:"post",
                url: link,
                async: false,
                success: function (result) 
				{ 
					action(result,page); 
				},
                
               
            }
            );

}

function succes2(result,page)
{
    $("#page" + page).append(result);
    page2I = true;
    scrollPage("page2");
 
}

function succes3(result,page)
{
    $("#page" + page).append(result);
    page3I = true;
    scrollPage("page3");
    servicios();
}
function succes4(result,page)
{
    $("#page" + page).append(result);
    page4I = true;
    scrollPage("page4");
    
}
function succes5(result,page)
{
    $("#page" + page).append(result);
    page5I = true;
    scrollPage("page5");
}
function succes6(result,page)
{
    $("#page" + page).append(result);
    page6I = true;
    scrollPage("page6");
}


function escroleatizarUno() {
    $('#slides').superslides({
        play: false,
        slide_easing: 'easeInOutCubic',
        slide_speed: 800,
        pagination: true,
        hashchange: false,
        scrollable: true, // Allow scrollable content inside slide
        nav: 'slides-navigation',
    });
}

//recorren el arreglo buscando la palabara, o plabra similar mal enetendida, para
//despues ejecutar la accion corespondiente.


function initializeTSounds()
{
   
    if (lenguaje == "es")
    {
        t1 = new buzz.sound("sounds/espanol/tour/t1", {formats:["mp3","ogg"]});
        t2 = new buzz.sound("sounds/espanol/tour/t2", {formats:["mp3","ogg"]});
        t3 = new buzz.sound("sounds/espanol/tour/t3", { formats: ["mp3", "ogg"] });
        t4 = new buzz.sound("sounds/espanol/tour/t4", { formats: ["mp3", "ogg"] });
    }
    else
    {
        t1 = new buzz.sound("sounds/english/tour/t1", { formats: ["mp3", "ogg"] });
        t2 = new buzz.sound("sounds/english/tour/t2", { formats: ["mp3", "ogg"] });
        t3 = new buzz.sound("sounds/english/tour/t3", { formats: ["mp3", "ogg"] });
        t4 = new buzz.sound("sounds/english/tour/t4", { formats: ["mp3", "ogg"] });
    }
        arrTSounds = new Array(t1, t2, t3,t4);
      
        t1.bind("ended", function ()
        {
            indexT++;
        goToT2(1);
        
    });
    t2.bind("ended", function () {
        indexT++;
        goToT3(1);
       

    });
    t3.bind("ended", function ()
    {
        indexT++;
        goToT4(1);
        
      
    });

    t4.bind("ended", function () {
        cancelTour();
    });
    tInitialized = true;
}

function startTour() {

    if (!tInitialized) {
        initializeTSounds();

    }

    if (!voiceSesion && !touring&&!helping)
    {
        touring = true;
		$("#btnTour").addClass("btnTour");
		$("#control").addClass("control");
		$("#control").removeClass("nocontrol");
		$(".letramenu").addClass("colorL");
        goToT1(1);
    }
	else
	{
		cancelTour();
		cancelHelp();
		cancelVoice();
	}
}
function startVHelp()
{
    apprise(compatiblidadChrome);
}
function startVoice()
{
    apprise(compatiblidadChrome);
}
function nextStation() {
    if (touring)
    {
        if (arrTSounds.indexOf(actualTSound) < arrTSounds.length - 1) {
            actualTSound.stop();
            stopMove();
            switch (indexT) {
                case 0:
                    indexT++;
                    goToT2(1);
                    break;
                case 1:
                    indexT++;
                    goToT3(1);
                    break;
                case 2:
                    goToT4(1);
                    break

            }
        }
    }
}

function prevStation() {
    if (arrTSounds.indexOf(actualTSound) != 0) {
        actualTSound.stop();
        stopMove();
        switch (indexT) {
            case 1:
                indexT--;
                goToT1(0);
                break;
            case 2:
                indexT--;
                goToT2(0);
                break;
            case 3:
                goToT3(0);
                break
        }
    }
}
function t1Move()
{
    i1 = setInterval(function ()
    {
        $("#next1").click();
        i2 = setInterval(function () {
            $("#next1").click();
            clearInterval(i2);
        }, 1000);
        clearInterval(i1);
    }, 7000);
}

function t3Move()
{
    i3 = setInterval(function ()
    {
        sliderC.next();
    
    },
    2500);
    setTimeout(function ()
    {
        clearInterval(i3);
    }, 11000);
}

function goToT4()
{
        playNextT();
        getPage6();
}

function goToT1(direction) {
    getPage2();
    if (direction == 1) {
            playNextT();
        }
        else {
            playBackT();
        }
        t1Move();
     
}	


function goToT2(direction) {
   
 
        if (direction == 1) {
            playNextT();
        }
        else
        {
            playBackT();
        }
        getPage3();
}

function goToT3(direction) {

   
 
        if (direction == 1)
        {
            playNextT();

        }
        else
        {
            playBackT();
        }
        getPage4();
        t3Move();

}

function ppTour() {
    if (actualTSound != null) {
        actualTSound.togglePlay();
    }
}

function playBackT() 
{

    actualTSound = arrTSounds[indexT];
    arrTSounds[indexT].play();

}

function stopMove()
{
    switch (ant) {
        case "page2":
            clearInterval(i1);
            clearInterval(i2);
            break;
        case "page4":
            clearInterval(i3);
            break;
    }
}

function cancelTour()
{
	if(touring)
	{
		stopMove();
		actualTSound.stop();
		actualTSound = arrTSounds[0];
		touring = false;
		indexT = 0;
		$("#btnTour").removeClass("btnTour");
		$("control").removeClass("control");
		$("control").addClass("nocontrol");
        $(".letramenu").removeClass("colorL");
	}
	  
}

function playNextT()
{
   
    if (indexT < arrTSounds.length)
    {
        actualTSound = arrTSounds[indexT];
        arrTSounds[indexT].play();

    }
}


//otros
function setService(service)
{
$(".service").addClass("serviceInactive");
$("#" + service).removeClass("serviceInactive");
}


function setTeam(team)
{
    $(".team").addClass("teamInactive");
    $("#" + team).removeClass("teamInactive");
    currentTm = teamM.indexOf(team);
}



//Servicio Movil movimiento
function mobilein(nombrebtn)
{
var division = $('.' + nombrebtn);
division.css({left:"23%"});
mobileOut(currentMovil);
currentMovil = nombrebtn;
}
function mobileOut(nombrebtn)
{
if (currentMovil) {
var division = $('.' + nombrebtn);
division.css({ left: "-63%" });
}
}





function servicios()
{
    scrollPage('page3');
    subirCortina();
	if(!opi)
	{
	opi=true;
	$('.slide').addClass('visible');
	}
}



var alpha=0;




//Inicio de isotope
function iniport()
 {
		
		//servicios marketing
		var Cmarketing = $('.todo-marketing');
		var h = $(window).height() - $(window).height()*0.01 - $('.top-bar').height() + '';
		Cmarketing.css({ height:h});
		
		
			var portafolio = $('#productos');
			var a = $(window).height() - $(window).height()*0.2  - $('.top-bar').height() + '';
			portafolio.css({ height:a});
		
		window.onresize = function(event)
		{
		    var portafolio = $('#productos');
		    var cortina = $("#cortina");
		    var h = $(window).height();
		    cortina.css({top:h*-1});
			var a = h - $(window).height()*0.2  - $('.top-bar').height() + '';
			portafolio.css({ height:a});
		}

 
 }
 
//------------------------------------------------------------
