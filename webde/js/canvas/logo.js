// animation globals
// animation globals
var t=0; 
var frameInterval = 25; // in ms
var canvas = null; // canvas DOM object
var context = null; // canvas context
 
// ball globals
var ballRadius = 1;
 
// physics globals
var collisionDamper = 0.3;
var floorFriction = 0.0005 * frameInterval;
var mouseForceMultiplier = 1 * frameInterval;
var restoreForce =0.002 * frameInterval;
 
var mouseX = 99999;
var mouseY = 99999;
 
var balls = null;
var wInterval; 

var wPos; 
var wInitialized=false;
var wait = 26;
var waited=false;
var wSInterval;
function Ball(x,y,vx,vy,color) 
{
    this.x=x;
    this.y=y;
    this.vx=vx;
    this.vy=vy;
    this.color=color;
 
    this.origX = x;
    this.origY = y;
  
}

function init() {
    $('.wCanvas').attr(
    {
	
    onmousemove:"handleMouseMove(event)", onmouseout:"handleMouseOut()"});
    canvas = document.getElementById("myCanvas");
  
    context=canvas.getContext("2d");
    initStageObjects();
    drawStageObjects();

}
function wAllTrue()
{
	var all=true;
	for(var i=0; i<wPos.length; i++)
	{
 		if(!wPos[i])
		{
		   all=false; 
		   break;
		}
	}
    return all;
}
 
function updateStage() 
{
    t+=frameInterval; 
    clearCanvas();
    updateStageObjects();
    drawStageObjects(); 
    
}



 
function initStageObjects() {
    balls = new Array();
	
    var blue = "#3A5BCD";
    var red="#EF2B36";
    var yellow = "#FEB300";
    var green="#02A817";
 
    // w
    balls.push(new Ball(5,5,0,0,yellow));
    balls.push(new Ball(16,5,0,0,yellow));
    balls.push(new Ball(5,16,0,0,yellow));
    balls.push(new Ball(16,16,0,0,yellow));
    balls.push(new Ball(16,27,0,0,yellow));
    balls.push(new Ball(16,38,0,0,yellow));
    balls.push(new Ball(27,38,0,0,yellow));
	balls.push(new Ball(16,49,0,0,yellow));
	balls.push(new Ball(27,49,0,0,yellow));
	balls.push(new Ball(16,60,0,0,yellow));
	balls.push(new Ball(27,60,0,0,yellow));
	balls.push(new Ball(27,71,0,0,yellow));
	balls.push(new Ball(38,71,0,0,yellow));
	balls.push(new Ball(38,60,0,0,yellow));
	balls.push(new Ball(49,60,0,0,yellow));
	balls.push(new Ball(49,49,0,0,yellow));
	balls.push(new Ball(49,38,0,0,yellow));
	balls.push(new Ball(49,27,0,0,yellow));
	balls.push(new Ball(49,16,0,0,yellow));
	balls.push(new Ball(60,5,0,0,yellow));
	balls.push(new Ball(60,16,0,0,yellow));
	balls.push(new Ball(60,27,0,0,yellow));
	balls.push(new Ball(60,38,0,0,yellow));
	balls.push(new Ball(71,16,0,0,yellow));
	balls.push(new Ball(71,27,0,0,yellow));
	balls.push(new Ball(71,38,0,0,yellow));
	balls.push(new Ball(71,49,0,0,yellow));
	balls.push(new Ball(71,60,0,0,yellow));
	balls.push(new Ball(82,60,0,0,yellow));
	balls.push(new Ball(82,71,0,0,yellow));
	balls.push(new Ball(93,49,0,0,yellow));
	balls.push(new Ball(104,49,0,0,yellow));
	balls.push(new Ball(93,38,0,0,yellow));
	balls.push(new Ball(104,38,0,0,yellow));
	balls.push(new Ball(104,27,0,0,yellow));
	balls.push(new Ball(104,16,0,0,yellow));
	balls.push(new Ball(115,16,0,0,yellow));
	balls.push(new Ball(104,5,0,0,yellow));
	balls.push(new Ball(115,5,0,0,yellow));

   
 
    
 
}
 
function drawStageObjects() {

    for (var n=0; n<balls.length; n++) {    
        context.beginPath();
        context.rect(balls[n].x,balls[n].y,9,9,false);
        context.fillStyle=balls[n].color;
        context.fill();


    }
 
 
}
 
function updateStageObjects() 
{
 

  
    	for (var n=0; n<balls.length; n++) 
	{
	
        // set ball position based on velocity
        balls[n].y+=balls[n].vy;
        balls[n].x+=balls[n].vx;
 
        // restore forces
 
 
 
        if (balls[n].x > balls[n].origX) {
            balls[n].vx -= restoreForce;
        }
        else {
            balls[n].vx += restoreForce;
        }
        if (balls[n].y > balls[n].origY) {
            balls[n].vy -= restoreForce;
        }
        else {
            balls[n].vy += restoreForce;
        }
 
 
 
        // mouse forces
        var distX = balls[n].x - mouseX;
        var distY = balls[n].y - mouseY;
 
        var radius = Math.sqrt(Math.pow(distX,2) + 
            Math.pow(distY,2));
 
        var totalDist = Math.abs(distX) + Math.abs(distY);
 
        var forceX = (Math.abs(distX) / totalDist) * 
            (1/radius) * mouseForceMultiplier;
        var forceY = (Math.abs(distY) / totalDist) * 
            (1/radius) * mouseForceMultiplier;
 
        if (distX>0) { // mouse is left of ball
            balls[n].vx += forceX;
        }
        else {
            balls[n].vx -= forceX;
        }
        if (distY>0) { // mouse is on top of ball
            balls[n].vy += forceY;
        }
        else {
            balls[n].vy -= forceY;
        }
 
 
        // floor friction
        if (balls[n].vx>0) {
            balls[n].vx-=floorFriction;
        }
        else if (balls[n].vx<0) {
            balls[n].vx+=floorFriction;
        }
        if (balls[n].vy>0) {
            balls[n].vy-=floorFriction;
        }
        else if (balls[n].vy<0) {
            balls[n].vy+=floorFriction;
        }
 
        // floor condition
        if (balls[n].y > (canvas.height-ballRadius)) 
	{
            balls[n].y=canvas.height-ballRadius-2;
            balls[n].vy*=-1; 
            balls[n].vy*=(1-collisionDamper);
        }
 
        // ceiling condition
        if (balls[n].y < (ballRadius)) {
            balls[n].y=ballRadius+2;
            balls[n].vy*=-1; 
            balls[n].vy*=(1-collisionDamper);
        }
 
        // right wall condition
        if (balls[n].x > (canvas.width-ballRadius)) {
            balls[n].x=canvas.width-ballRadius-2;
            balls[n].vx*=-1;
            balls[n].vx*=(1-collisionDamper);
        }
 
        // left wall condition
        if (balls[n].x < (ballRadius)) {
            balls[n].x=ballRadius+2;
            balls[n].vx*=-1;
            balls[n].vx*=(1-collisionDamper);
        }   
    
	var wx = Math.floor(balls[n].x);
	var wy = Math.floor(balls[n].y);
	if((wx>=balls[n].origX-1 && wx<=(balls[n].origX+1)) && (wy>=balls[n].origY-1&&wy<=(balls[n].origY+1)))
	{
	 
	 wPos[n]=true;
	}
	else
	{
	 wPos[n]=false;
	}
    }
   //console.log(wAllTrue());
   var f=function(){
   if(wAllTrue())
    {
	wInitialized=false;
	waited=false;
	clearInterval(wInterval);
	clearInterval(wSInterval);
	
   }
};
    if(waited)
	{
		
		f();
	};


}

 
function clearCanvas() 
{
    context.clearRect(0,0,canvas.width, canvas.height);

}
 
function shake()
{
  if(!wInitialized)
   {
      wPos= new Array(balls.length);
      mouseX = canvas.width-50;
      mouseY = canvas.height/3;
      wSInterval= setInterval(updateStage, frameInterval);
      setTimeout(function(){waited=true}, 3000);
      setTimeout(handleMouseOut, 250);
     wInitialized=true;
   }
}
function handleMouseMove(evt) 
{
   if(!wInitialized)
   {
      
      wPos= new Array(balls.length);
      wInterval= setInterval(updateStage, frameInterval);
      wInitialized=true;
      setTimeout(function(){waited=true}, 100);
   }
   mouseX = evt.clientX - canvas.offsetLeft;
   mouseY = evt.clientY - canvas.offsetTop;
   	
}
 
function handleMouseOut() 
{
   mouseX = 99999;
   mouseY = 99999;
}